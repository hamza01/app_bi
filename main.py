import os
import sys


def chose():
    
    print("a - CategoricalNB")
    print("b - LogisticRegression")
    print("c - SVM")
    print("d - KNN")
    print("e ----- exit")
    x = str(input("Choisi votre classifieur  : "))

    if x == 'a':
        print("vous avez choisi  : Classifieur Bayésien Naïf - Catégorical NB")
        os.system('python CategoricalNB.py')
        chose()
    elif x == 'b':
        print("vous avez choisi  : Modèle de régression logistique - LR")
        os.system('python LogisticRegression.py')
        chose()
    elif x == 'c':
        print("vous avez choisi  : Séparateur a vaste marge")
        os.system('python SVM.py')
        chose()
    elif x == "d":
        print("vous avez choisi  : Méthode des k plus proches voisins")
        os.system('python knn.py')
        chose()
    else:
        exit()

chose()

