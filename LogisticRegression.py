import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from pandas.plotting import scatter_matrix
from matplotlib import cm
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.dummy import DummyClassifier
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, recall_score, precision_score, roc_curve, auc, log_loss
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import GridSearchCV
from sklearn import svm
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.metrics import accuracy_score, classification_report, f1_score, precision_score, recall_score
from currency_converter import CurrencyConverter
from datetime import date
import datetime
import category_encoders as ce
import time
from sklearn.feature_selection import RFE
from collections import Counter
from imblearn.over_sampling import SMOTE





c = CurrencyConverter()

plt.ioff()

#Exploration ---------------------------

df = pd.read_csv("donnees/projects.csv",sep=",", encoding='cp1252')





#Nettoyage ---------------------------

def currency_convertor(row,new_currency,h):
 goal = row['goal']
 pledged = row['pledged']
 curr = row['currency']
 if h == 'g':
    new_curr = c.convert(goal,curr,new_currency)
    return int(new_curr)
 else:
    # new_pledged = c.convert(pledged,curr,new_currency,date=date(2013, 3, 21))
    new_pledged = c.convert(pledged,curr,new_currency)
    return int(new_pledged)

df['new_goal'] = df.apply(lambda x: currency_convertor(x,"EUR",'g'), axis=1)
# df['new_pledged'] = df.apply(lambda y: currency_convertor(y,"EUR",'p'), axis=1)
df = df.drop(['goal'], axis=1) #drop two columns : goal & pledged

col_names = df.columns

# categorical = [var for var in df.columns if df[var].dtype=='O']
# print('There are {} categorical variables\n'.format(len(categorical)))
# print('The categorical variables are : ', categorical)
# print(df[categorical].head())

# print(df[categorical].isnull().sum())

# for var in categorical:
    
#     print(var, ' contains ', len(df[var].unique()), ' labels')

df['duree'] = (pd.to_datetime(df['end_date']) - pd.to_datetime(df['start_date'])) / np.timedelta64(1, 'D')
df['duree'] = df['duree'].apply(np.int64)

df['end_date'] = pd.to_datetime(df['end_date'], infer_datetime_format=True)
df['start_date'] = pd.to_datetime(df['start_date'], infer_datetime_format=True)

# df['Year_start'] = df['start_date'].dt.year
# df['Month_start'] = df['start_date'].dt.month
# df['Day_start'] = df['start_date'].dt.day

# df['Year_end'] = df['end_date'].dt.year
# df['Month_end'] = df['end_date'].dt.month
# df['Day_end'] = df['end_date'].dt.day



df.drop(['end_date','start_date'], axis=1, inplace = True)

categorical = [var for var in df.columns if df[var].dtype=='O']
# print('There are {} categorical variables\n'.format(len(categorical)))
# print('The categorical variables are : ', categorical)

# print(df[categorical].isnull().sum())

# pd.get_dummies(df.name, drop_first=True, dummy_na=True)
pd.get_dummies(df.country, drop_first=True, dummy_na=True)
pd.get_dummies(df.sex, drop_first=True, dummy_na=True)

numerical = [var for var in df.columns if df[var].dtype!='O']
# print('There are {} numerical variables\n'.format(len(numerical)))
# print('The numerical variables are :', numerical)
# print(df[numerical].head())

# print(df[numerical].isnull().sum())

df['new_sex'] = np.where(df['sex'] == "female", 1, 0)
# df.set_index('id',inplace = True)

df.loc[df.state == "canceled", "state"] = "failed"
df.loc[df.state == "suspended", "state"] = "failed"
df = df[df.state != 'live']
df = df[df.state != 'undefined']


df['state'] = np.where(df['state'] == "successful", 1, 0)





# print(df.head())


X = df.drop(['backers','name','sex','state',"pledged",'id'], axis=1)
y = df['state']

# print(X.head())


# # Découpage ---------------------------



X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=1)

print(X_train.shape) 
print(X_test.shape)

# print(X_train.dtypes)




# print(X_train[categorical].isnull().mean())

# for col in categorical:
#     if X_train[col].isnull().mean()>0:
#         print(col, (X_train[col].isnull().mean()))



for df2 in [X_train, X_test]:
    df2['country'].fillna(X_train['country'].mode()[0], inplace=True)
    df2['currency'].fillna(X_train['currency'].mode()[0], inplace=True)


categorical = [col for col in X_train.columns if X_train[col].dtypes == 'O']
# print(categorical)
numerical = [col for col in X_train.columns if X_train[col].dtypes != 'O']
# print(numerical)

X_train = pd.concat([pd.get_dummies(X_train.category),
                     pd.get_dummies(X_train.subcategory),
                     pd.get_dummies(X_train.country)], axis=1)


X_test = pd.concat([pd.get_dummies(X_test.category),
                     pd.get_dummies(X_test.subcategory),
                     pd.get_dummies(X_test.country)], axis=1)


# print(X_train.head())
# print(X_test.head())

cols = X_train.columns

scaler = MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)


X_train = pd.DataFrame(X_train, columns=[cols])
X_test = pd.DataFrame(X_test, columns=[cols])

# smote = SMOTE()

# print('Original dataset shape', Counter(y_train))

# X_train,y_train = smote.fit_resample(X_train,y_train)

# print('Resample dataset shape', Counter(y_train))

start = time.time()

logreg = LogisticRegression(C=100, solver='liblinear', random_state=0)
logreg.fit(X_train, y_train)
y_pred_test = logreg.predict(X_test)
print('Model accuracy score : {0:0.4f}'. format(accuracy_score(y_test, y_pred_test)))

print(classification_report(y_test, y_pred_test))

matrix = confusion_matrix(y_test, y_pred_test)

print('Confusion matrix\n\n', matrix)

print('\nTrue Positives(TP) = ', matrix[0,0])

print('\nTrue Negatives(TN) = ', matrix[1,1])

print('\nFalse Positives(FP) = ', matrix[0,1])

print('\nFalse Negatives(FN) = ', matrix[1,0])

# precision = matrix[0,0] / (matrix[0,0] + matrix[0,1])
# recall = matrix[0,0] / (matrix[0,0] + matrix[1,0])
# F1Score = 2 / ((1/precision) + (1/recall))
# print("precision matrix : ", precision)
# print("recall matrix : ", recall)
# print("F1-Score matrix : ", F1Score)

print("\ n")
print("\ n")
print("\ n")


print("Recall score : ", recall_score(y_test, y_pred_test , average='micro'))
print("Precision score : ",precision_score(y_test, y_pred_test , average='micro'))
print("F1 score : ",f1_score(y_test, y_pred_test , average='micro'))

end = time.time()
print((end - start)/60, " minutes --------------------")






























# data.dropna(subset = ['country'], inplace = True)
# data.dropna(subset = ['sex'], inplace = True)

# data.loc[data.state == "canceled", "state"] = "failed"
# data.loc[data.state == "suspended", "state"] = "failed"
# data = data[data.state != 'live']

# data['duree'] = (pd.to_datetime(data['end_date']) - pd.to_datetime(data['start_date'])) / np.timedelta64(1, 'D')
# data['duree'] = data['duree'].apply(np.int64)

# print(data['sex'].value_counts())

# data['new_sex'] = np.where(data['sex'] == "female", 1, 0) 
# data['state'] = np.where(data['state'] == "successful", 1, 0)
# data.set_index('id',inplace = True)


# nominal = ['category','country','subcategory']
# dataProcessed = pd.get_dummies(data,columns=nominal)
