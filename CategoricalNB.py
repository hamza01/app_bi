import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from pandas.plotting import scatter_matrix
from matplotlib import cm
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.dummy import DummyClassifier
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, recall_score, precision_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, RobustScaler
from sklearn.model_selection import GridSearchCV
from sklearn import svm
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.metrics import accuracy_score, classification_report, f1_score
from currency_converter import CurrencyConverter
from datetime import date
import datetime
import category_encoders as ce
import time
from sklearn.naive_bayes import CategoricalNB
from sklearn.preprocessing import OrdinalEncoder, LabelEncoder
from statsmodels.stats.outliers_influence import variance_inflation_factor
from imblearn.over_sampling import SMOTE
from collections import Counter







c = CurrencyConverter()

plt.ioff()

#Exploration ---------------------------

data = pd.read_csv("donnees/projects.csv",sep=",", encoding='cp1252')


#Nettoyage ---------------------------

def currency_convertor(row,new_currency,h):
 goal = row['goal']
 pledged = row['pledged']
 curr = row['currency']
 if h == 'g':
    new_curr = c.convert(goal,curr,new_currency)
    return int(new_curr)
 else:
    # new_pledged = c.convert(pledged,curr,new_currency,date=date(2013, 3, 21))
    new_pledged = c.convert(pledged,curr,new_currency)
    return int(new_pledged)


data['new_goal'] = data.apply(lambda x: currency_convertor(x,"EUR",'g'), axis=1)
data['new_pledged'] = data.apply(lambda y: currency_convertor(y,"EUR",'p'), axis=1)

data = data.drop(['goal','pledged'], axis=1) #drop two columns : goal & pledged
# data.drop(data[data['state'] == "live")].index, inplace=True)



data.dropna(subset = ['country'], inplace = True) #drop all nan value in the country column
data.dropna(subset = ['sex'], inplace = True) #drop all nan value in the sex column
data.dropna(subset = ['name'], inplace = True)

data.loc[data.state == "canceled", "state"] = "failed"
data.loc[data.state == "suspended", "state"] = "failed"
data = data[data.state != 'live']
data = data[data.state != 'undefined']




#get number of days
data['duree'] = (pd.to_datetime(data['end_date']) - pd.to_datetime(data['start_date'])) / np.timedelta64(1, 'D')
data['duree'] = data['duree'].apply(np.int64)


# print(data.isnull().sum())

data.set_index('id',inplace = True)

features = data.columns.tolist()
features.remove('state')
# print('----------- features -------------')
# print(features)

encoder = OrdinalEncoder()
data_encoded = encoder.fit_transform(data[features])
car_df_encoded = pd.DataFrame(data_encoded, columns=features)
# print('----------- OrdinalEncoder -------------')
# print(data_encoded)



encoder = LabelEncoder()
target_encoded = encoder.fit_transform(data['state'])
car_df_encoded['state'] = target_encoded
encoder.inverse_transform(target_encoded)


vif = pd.DataFrame()
vif["VIF"] = [variance_inflation_factor(car_df_encoded.values, i) for i in range(len(features))]
vif["Features"] = features
# print(vif)


# print(car_df_encoded.dtypes)
# print(data.duree.value_counts())
# print(data.age.value_counts())



X = car_df_encoded.drop(['name','start_date','end_date','state',"new_pledged",'backers','duree'], axis=1).values
y = car_df_encoded['state'].values



# Découpage ---------------------------


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=1)



print(X_train.shape) 
print(X_test.shape)


# smote = SMOTE()

# print('Original dataset shape', Counter(y_train))

# X_train,y_train = smote.fit_resample(X_train,y_train)

# print('Resample dataset shape', Counter(y_train))

cnb = CategoricalNB(min_categories=9)



print(' ------------ CategoricalNB ------------')

start = time.time()

cnb.fit(X_train, y_train)
prediction1 = cnb.predict(X_test)
precision1 = accuracy_score(y_test, prediction1, normalize = True)
print("Accuracy : ", precision1)
print(classification_report(y_test, prediction1))

matrix = confusion_matrix(y_test, prediction1)
print('Confusion matrix\n\n', matrix)
print('\nTrue Positives(TP) = ', matrix[0,0])
print('\nTrue Negatives(TN) = ', matrix[1,1])
print('\nFalse Positives(FP) = ', matrix[0,1])
print('\nFalse Negatives(FN) = ', matrix[1,0])

print("\ n")
print("\ n")
print("\ n")

print("Recall score : ", recall_score(y_test, prediction1 , average='micro'))
print("Precision score : ",precision_score(y_test, prediction1 , average='micro'))
print("F1 score : ",f1_score(y_test, prediction1 , average='micro'))

# precision = matrix[0,0] / (matrix[0,0] + matrix[0,1])
# recall = matrix[0,0] / (matrix[0,0] + matrix[1,0])
# F1Score = 2 / ((1/precision) + (1/recall))
# print("precision  : ", precision)
# print("recall  : ", recall)
# print("F1-Score  : ", F1Score)

end = time.time()
print((end - start)/60, "minutes ----------")

# print("\ n")
# print("\ n")
# print("\ n")

# print("Recall score : ", recall_score(y_test, prediction1 , average='micro'))
# print("Precision score : ",precision_score(y_test, prediction1 , average='micro'))
# print("F1 score : ",f1_score(y_test, prediction1 , average='micro'))


