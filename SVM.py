import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from pandas.plotting import scatter_matrix
from matplotlib import cm
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.dummy import DummyClassifier
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, recall_score, precision_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import GridSearchCV
from sklearn import svm
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.metrics import accuracy_score, classification_report, f1_score
from currency_converter import CurrencyConverter
from datetime import date
import datetime
import category_encoders as ce
import time
from sklearn.naive_bayes import CategoricalNB



c = CurrencyConverter()

plt.ioff()

#Exploration ---------------------------

data = pd.read_csv("donnees/projects.csv",sep=",", encoding='cp1252')




#Nettoyage ---------------------------

def currency_convertor(row,new_currency,h):
 goal = row['goal']
 pledged = row['pledged']
 curr = row['currency']
 if h == 'g':
    new_curr = c.convert(goal,curr,new_currency)
    return int(new_curr)
 else:
    # new_pledged = c.convert(pledged,curr,new_currency,date=date(2013, 3, 21))
    new_pledged = c.convert(pledged,curr,new_currency)
    return int(new_pledged)


data['new_goal'] = data.apply(lambda x: currency_convertor(x,"EUR",'g'), axis=1)
data['new_pledged'] = data.apply(lambda y: currency_convertor(y,"EUR",'p'), axis=1)

data = data.drop(['goal','pledged'], axis=1) #drop two columns : goal & pledged


data.dropna(subset = ['country'], inplace = True) #drop all nan value in the country column
data.dropna(subset = ['sex'], inplace = True) #drop all nan value in the sex column

data.loc[data.state == "canceled", "state"] = "failed"
data.loc[data.state == "suspended", "state"] = "failed"
data = data[data.state != 'live']
data = data[data.state != 'undefined']



#get number of days
data['duree'] = (pd.to_datetime(data['end_date']) - pd.to_datetime(data['start_date'])) / np.timedelta64(1, 'D')
data['duree'] = data['duree'].apply(np.int64)


#Recodage ---------------------------

data['new_sex'] = np.where(data['sex'] == "female", 1, 0) #add column of sex but with 1 & 0 , 1 for female and 0 for male, numérisation d’attributs catégoriels.
data['state'] = np.where(data['state'] == "successful", 1, 0)


data.set_index('id',inplace = True)
nominal = ['category','country','subcategory','currency']
dataProcessed = pd.get_dummies(data,columns=nominal)



X = dataProcessed.drop(['name','sex','start_date','end_date','state',"new_pledged",'backers'], axis=1)
y = data['state']




X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=1)



print(X_train.shape) 
print(X_test.shape)

scaler = MinMaxScaler()

cols = X_train.columns

X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

clfSvm = SVC(kernel = 'rbf', random_state = 0)



start = time.time()

print(' ------------ SVM ------------')

clfSvm.fit(X_train, y_train)
prediction2 = clfSvm.predict(X_test)
print(prediction2)
precision2 = accuracy_score(y_test, prediction2)
print("Accuracy : ", precision2)
print(classification_report(y_test, prediction2))

matrix = confusion_matrix(y_test, prediction2)

print('Confusion matrix\n\n', matrix)


print('\nTrue Positives(TP) = ', matrix[0,0])

print('\nTrue Negatives(TN) = ', matrix[1,1])

print('\nFalse Positives(FP) = ', matrix[0,1])

print('\nFalse Negatives(FN) = ', matrix[1,0])




end = time.time()
print((end - start)/60, " minutes ------------")




